package edu.cmu.ds.pbolar.privacymonitor.server.entity.azure;

/**
 *
 * @author Pranav
 */
public class Caption {
    
    private String text;
    private Double confidence;

    public Caption(){}

    public Caption(String text, Double confidence) {
        this.text = text;
        this.confidence = confidence;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Double getConfidence() {
        return confidence;
    }

    public void setConfidence(Double confidence) {
        this.confidence = confidence;
    }

    @Override
    public String toString() {
        return "Caption{" +
                "text='" + text + '\'' +
                ", confidence=" + confidence +
                '}';
    }

}
