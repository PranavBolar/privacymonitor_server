package edu.cmu.ds.pbolar.privacymonitor.server.entity;

/**
 *
 * @author Pranav
 */
public class RemoteServerResponse {
    
    private long responseTime;
    private String content;
    private int status;

    public RemoteServerResponse(int status, long responseTime) {
        this.status = status;
        this.responseTime = responseTime;
    }
    
    public RemoteServerResponse(int status, long responseTime, String content) {
        this.content = content;
        this.status = status;
        this.responseTime = responseTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(long responseTime) {
        this.responseTime = responseTime;
    }
    
    @Override
    public String toString() {
        return "ServerResponse{" + "content=" + content + ", status=" + status + '}';
    }

    
    
}
