package edu.cmu.ds.pbolar.privacymonitor.server.entity.azure;

public class FaceRectangle {

    private Integer left, top, width, height;

    public FaceRectangle() {}

    public FaceRectangle(Integer left, Integer top, Integer width, Integer height) {
        this.left = left;
        this.top = top;
        this.width = width;
        this.height = height;
    }

    public Integer getLeft() {
        return left;
    }

    public void setLeft(Integer left) {
        this.left = left;
    }

    public Integer getTop() {
        return top;
    }

    public void setTop(Integer top) {
        this.top = top;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FaceRectangle that = (FaceRectangle) o;

        if (!left.equals(that.left)) return false;
        if (!top.equals(that.top)) return false;
        if (!width.equals(that.width)) return false;
        return height.equals(that.height);
    }

    @Override
    public int hashCode() {
        int result = left.hashCode();
        result = 31 * result + top.hashCode();
        result = 31 * result + width.hashCode();
        result = 31 * result + height.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "FaceRectangle{" +
                "left=" + left +
                ", top=" + top +
                ", width=" + width +
                ", height=" + height +
                '}';
    }
}
