package edu.cmu.ds.pbolar.privacymonitor.server.entity.azure;

public class AdultRating {

    private Boolean isAdultContent, isRacyContent;
    private Double adultScore, racyScore;

    public AdultRating() {}

    public AdultRating(Boolean isAdultContent, Boolean isRacyContent, Double adultScore, Double racyScore) {
        this.isAdultContent = isAdultContent;
        this.isRacyContent = isRacyContent;
        this.adultScore = adultScore;
        this.racyScore = racyScore;
    }

    public Boolean getAdultContent() {
        return isAdultContent;
    }

    public void setAdultContent(Boolean adultContent) {
        isAdultContent = adultContent;
    }

    public Boolean getRacyContent() {
        return isRacyContent;
    }

    public void setRacyContent(Boolean racyContent) {
        isRacyContent = racyContent;
    }

    public Double getAdultScore() {
        return adultScore;
    }

    public void setAdultScore(Double adultScore) {
        this.adultScore = adultScore;
    }

    public Double getRacyScore() {
        return racyScore;
    }

    public void setRacyScore(Double racyScore) {
        this.racyScore = racyScore;
    }

    @Override
    public String toString() {
        return "AdultRating{" +
                "isAdultContent=" + isAdultContent +
                ", isRacyContent=" + isRacyContent +
                ", adultScore=" + adultScore +
                ", racyScore=" + racyScore +
                '}';
    }
}
