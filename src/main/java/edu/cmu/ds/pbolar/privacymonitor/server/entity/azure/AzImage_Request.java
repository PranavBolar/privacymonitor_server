package edu.cmu.ds.pbolar.privacymonitor.server.entity.azure;

public class AzImage_Request {

    private String url;

    public AzImage_Request(String url) {
        this.url = url;
    }

    public String toJson(){
        return this.toString();
    }

    @Override
    public String toString() {
        return String.format("{\"url\":\"%s\"}", url);
    }
}
