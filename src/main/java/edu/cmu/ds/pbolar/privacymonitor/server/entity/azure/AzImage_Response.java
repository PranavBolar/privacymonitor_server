package edu.cmu.ds.pbolar.privacymonitor.server.entity.azure;

import java.util.List;

public class AzImage_Response {

    private List<Category> categories;
    private List<Tag> tags;
    private AdultRating adult;
    private ImageDescription description;
    private String requestId;
    private ImageColour color;
    private List<Person> faces;

    public AzImage_Response() {}

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public AdultRating getAdult() {
        return adult;
    }

    public void setAdult(AdultRating adult) {
        this.adult = adult;
    }

    public ImageDescription getDescription() {
        return description;
    }

    public void setDescription(ImageDescription description) {
        this.description = description;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public ImageColour getColor() {
        return color;
    }

    public void setColor(ImageColour color) {
        this.color = color;
    }

    public List<Person> getFaces() {
        return faces;
    }

    public void setFaces(List<Person> faces) {
        this.faces = faces;
    }
    
    public boolean hasFaces(){
        return this.faces != null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AzImage_Response that = (AzImage_Response) o;

        if (categories != null ? !categories.equals(that.categories) : that.categories != null) return false;
        if (tags != null ? !tags.equals(that.tags) : that.tags != null) return false;
        if (adult != null ? !adult.equals(that.adult) : that.adult != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (!requestId.equals(that.requestId)) return false;
        if (color != null ? !color.equals(that.color) : that.color != null) return false;
        return faces != null ? faces.equals(that.faces) : that.faces == null;
    }

    @Override
    public int hashCode() {
        int result = categories != null ? categories.hashCode() : 0;
        result = 31 * result + (tags != null ? tags.hashCode() : 0);
        result = 31 * result + (adult != null ? adult.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + requestId.hashCode();
        result = 31 * result + (color != null ? color.hashCode() : 0);
        result = 31 * result + (faces != null ? faces.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AzImage_Response{" +
                "categories=" + categories +
                ", tags=" + tags +
                ", adult=" + adult +
                ", description=" + description +
                ", requestId='" + requestId + '\'' +
                ", color=" + color +
                ", faces=" + faces +
                '}';
    }
}
