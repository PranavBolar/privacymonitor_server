package edu.cmu.ds.pbolar.privacymonitor.server.entity.azure;

public class AzImage_Error {

    private String code, requestId, message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "AzImage_Error{" +
                "code='" + code + '\'' +
                ", requestId='" + requestId + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
