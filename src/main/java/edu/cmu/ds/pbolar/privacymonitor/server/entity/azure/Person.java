package edu.cmu.ds.pbolar.privacymonitor.server.entity.azure;

public class Person {

    private FaceRectangle faceRectangle;
    private String name, gender;
    private Integer age;
    private Double confidence;

    public Person() {}

    public Person(FaceRectangle faceRectangle, String name, String gender, Integer age, Double confidence) {
        this.faceRectangle = faceRectangle;
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.confidence = confidence;
    }

    public FaceRectangle getFaceRectangle() {
        return faceRectangle;
    }

    public void setFaceRectangle(FaceRectangle faceRectangle) {
        this.faceRectangle = faceRectangle;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }
    
    public boolean isMale(){
        return this.gender.equalsIgnoreCase("male");
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Double getConfidence() {
        return confidence;
    }

    public void setConfidence(Double confidence) {
        this.confidence = confidence;
    }

    @Override
    public String toString() {
        return "Person{" +
                "faceRectangle=" + faceRectangle +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", age=" + age +
                ", confidence=" + confidence +
                '}';
    }
}
