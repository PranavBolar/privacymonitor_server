package edu.cmu.ds.pbolar.privacymonitor.server.entity;

import java.io.Serializable;

/**
 *
 * @author Pranav
 */
public class AzureRequest_Url implements Serializable{
    private String url;

    public AzureRequest_Url(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "AzureRequest{" + "url=" + url + '}';
    }
    
}
