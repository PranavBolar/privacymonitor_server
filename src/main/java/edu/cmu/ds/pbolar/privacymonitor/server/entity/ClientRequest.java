package edu.cmu.ds.pbolar.privacymonitor.server.entity;

/**
 *
 * @author Pranav
 */
public class ClientRequest {
    
    private Boolean isUrl;
    private String image_url, image_base64, phoneModel;

    public ClientRequest() {}

    public Boolean getIsUrl() {
        return isUrl;
    }

    public void setIsUrl(Boolean isUrl) {
        this.isUrl = isUrl;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getImage_base64() {
        return image_base64;
    }
    
    public boolean isImageUrl(){
        return this.isUrl;
    }

    public void setImage_base64(String image_base64) {
        this.image_base64 = image_base64;
    }

    public String getPhoneModel() {
        return phoneModel;
    }

    public void setPhoneModel(String phoneModel) {
        this.phoneModel = phoneModel;
    }

    @Override
    public String toString() {
        return "ClientRequest{" + "isUrl=" + isUrl + ", image_url=" + image_url + ", image_base64=" + image_base64 + ", phoneModel=" + phoneModel + '}';
    }

}
