package edu.cmu.ds.pbolar.privacymonitor.server.entity.azure;

import java.util.List;

public class CategoryDetail {

    private List<Celebrity> celebrities;
    private List<Tag> landmarks;

    public enum ID{
        CELEBRITY, LANDMARK
    }
    
    public CategoryDetail() {}

    public CategoryDetail(List<Celebrity> celebrities, List<Tag> landmarks) {
        this.celebrities = celebrities;
        this.landmarks = landmarks;
    }

    public List<Celebrity> getCelebrities() {
        return celebrities;
    }

    public void setCelebrities(List<Celebrity> celebrities) {
        this.celebrities = celebrities;
    }

    public List<Tag> getLandmarks() {
        return landmarks;
    }

    public void setLandmarks(List<Tag> landmarks) {
        this.landmarks = landmarks;
    }
    
    public boolean hasCelebs(){
        return this.celebrities!=null;
    }
    
    public boolean hasLandmarks(){
        return this.landmarks!=null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CategoryDetail that = (CategoryDetail) o;

        if (celebrities != null ? !celebrities.equals(that.celebrities) : that.celebrities != null) return false;
        return landmarks != null ? landmarks.equals(that.landmarks) : that.landmarks == null;
    }

    @Override
    public int hashCode() {
        int result = celebrities != null ? celebrities.hashCode() : 0;
        result = 31 * result + (landmarks != null ? landmarks.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CategoryDetail{" +
                "celebrities=" + celebrities +
                ", landmarks=" + landmarks +
                '}';
    }
}
