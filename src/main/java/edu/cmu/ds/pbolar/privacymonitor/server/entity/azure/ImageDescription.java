package edu.cmu.ds.pbolar.privacymonitor.server.entity.azure;

import java.util.List;

public class ImageDescription {

    private List<String> tags;
    private List<Caption> captions;

    public ImageDescription() {}

    public ImageDescription(List<String> tags, List<Caption> captions) {
        this.tags = tags;
        this.captions = captions;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<Caption> getCaptions() {
        return captions;
    }

    public void setCaptions(List<Caption> captions) {
        this.captions = captions;
    }

    @Override
    public String toString() {
        return "ImageDescription{" +
                "tags=" + tags +
                ", captions=" + captions +
                '}';
    }
}
