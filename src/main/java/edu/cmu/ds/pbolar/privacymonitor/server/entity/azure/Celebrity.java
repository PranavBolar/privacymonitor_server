package edu.cmu.ds.pbolar.privacymonitor.server.entity.azure;

public class Celebrity {

    private String name;
    private FaceRectangle faceRectangle;
    private Double confidence;

    public Celebrity() {}

    public Celebrity(String name, FaceRectangle faceRectangle, Double confidence) {
        this.name = name;
        this.faceRectangle = faceRectangle;
        this.confidence = confidence;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FaceRectangle getFaceRectangle() {
        return faceRectangle;
    }

    public void setFaceRectangle(FaceRectangle faceRectangle) {
        this.faceRectangle = faceRectangle;
    }

    public Double getConfidence() {
        return confidence;
    }

    public void setConfidence(Double confidence) {
        this.confidence = confidence;
    }

    @Override
    public String toString() {
        return "Celebrity{" +
                "name='" + name + '\'' +
                ", faceRectangle=" + faceRectangle +
                ", confidence=" + confidence +
                '}';
    }
}
