package edu.cmu.ds.pbolar.privacymonitor.server.entity.azure;

import java.util.List;

/**
 *
 * @author Pranav
 */
public class ImageColour {
    
    private String dominantColorForeground, dominantColorBackground, accentColor;
    private List<String> dominantColors;
    private Boolean isBWImg;

    public ImageColour() {}

    public ImageColour(String dominantColorForeground, String dominantColorBackground, String accentColor, List<String> dominantColors, Boolean isBWImg) {
        this.dominantColorForeground = dominantColorForeground;
        this.dominantColorBackground = dominantColorBackground;
        this.accentColor = accentColor;
        this.dominantColors = dominantColors;
        this.isBWImg = isBWImg;
    }

    public String getDominantColorForeground() {
        return dominantColorForeground;
    }

    public void setDominantColorForeground(String dominantColorForeground) {
        this.dominantColorForeground = dominantColorForeground;
    }

    public String getDominantColorBackground() {
        return dominantColorBackground;
    }

    public void setDominantColorBackground(String dominantColorBackground) {
        this.dominantColorBackground = dominantColorBackground;
    }

    public String getAccentColor() {
        return accentColor;
    }

    public void setAccentColor(String accentColor) {
        this.accentColor = accentColor;
    }

    public List<String> getDominantColors() {
        return dominantColors;
    }

    public void setDominantColors(List<String> dominantColors) {
        this.dominantColors = dominantColors;
    }

    public Boolean isBWImg() {
        return isBWImg;
    }

    public void setIsBWImg(Boolean BWImg) {
        isBWImg = BWImg;
    }

    @Override
    public String toString() {
        return "ImageColour{" +
                "dominantColorForeground='" + dominantColorForeground + '\'' +
                ", dominantColorBackground='" + dominantColorBackground + '\'' +
                ", accentColor='" + accentColor + '\'' +
                ", dominantColors=" + dominantColors +
                ", isBWImg=" + isBWImg +
                '}';
    }

}
