package edu.cmu.ds.pbolar.privacymonitor.server.model;

/**
 *
 * @author Pranav
 */
public class Constant {
    
    // Azure
    public static final String AZURE_SUB_KEY = "9edc58ef72f847e48c08321b7d47459e";
    private static final String AZ_COG_SVC_URI = "https://eastus.api.cognitive.microsoft.com/vision/v1.0/analyze";
    public static final String AZURE_SUB_KEY_HEADER = "Ocp-Apim-Subscription-Key";
    private static final String AZURE_VISION_FEATURES = 
            "?visualFeatures=Categories,Description,Color,Faces,Tags,Adult&details=Celebrities,Landmarks";

    // Mongo
    private static String mLabURI = "mongodb://%s:%s@ds012678.mlab.com:12678/%s";
    private static final String DB = "pbolar-ds-p4";
    private static final String USER = "pbolar";
    private static final String PASS = "rand0m"; // ARGHH CLEAR TEXT PASSWORD
    
    // Log keys
    public static final String MDB_TIMESTAMP_KEY = "timestamp"; // timestamp
    public static final String MDB_LATENCY_KEY = "latency";
    public static final String MDB_RESP_SIZE_KEY = "resp-size";
    public static final String MDB_RESP_TIME_KEY = "resp-time";
    public static final String MDB_TAG_NUM_KEY = "tag-num";
    public static final String MDB_CAPTION_CONFIDENCE_KEY = "caption-confidence";
    public static final String MDB_FACES_NUM_KEY = "face-num";
    public static final String MDB_LANDMARK_KEY = "landmark";
    public static final String MDB_CELEB_KEY = "celebrity";
    public static final String MDB_PHONE_KEY = "phone-model";
    public static final String MDB_AZURE_ERROR_KEY = "az-error";
    
    // MIMEs
    public static final String OCTET_STREAM_MIME = "application/octet-stream";
    public static final String JSON_MIME = "application/json";
    
    /**
     * 
     * @return  Returns the mLab URI to the database for this project
     */
    public static final String getMLabURI(){
        return String.format(mLabURI, USER, PASS, DB);
    }
    
    /**
     * 
     * @return Gets a qualified URL for Azure, with all required details and features
     */
    public static final String getAzureUri(){
        return AZ_COG_SVC_URI + AZURE_VISION_FEATURES;
    }
    
}
