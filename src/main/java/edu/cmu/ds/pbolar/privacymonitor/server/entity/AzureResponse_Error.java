package edu.cmu.ds.pbolar.privacymonitor.server.entity;

/**
 *
 * @author Pranav
 */
public class AzureResponse_Error {
    
    private String code, requestId, message;

    public AzureResponse_Error() {}

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "AzureResponse_Error{" + "code=" + code + ", requestId=" + requestId + ", message=" + message + '}';
    }
    
}
