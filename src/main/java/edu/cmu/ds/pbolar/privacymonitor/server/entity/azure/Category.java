package edu.cmu.ds.pbolar.privacymonitor.server.entity.azure;

public class Category {

    private String name;
    private Double score;
    private CategoryDetail detail;

    public Category() {}

    public Category(String name, Double score, CategoryDetail detail) {
        this.name = name;
        this.score = score;
        this.detail = detail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public CategoryDetail getDetail() {
        return detail;
    }

    public void setDetail(CategoryDetail detail) {
        this.detail = detail;
    }
    
    public boolean hasDetail(){
        return this.detail!=null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Category category = (Category) o;

        if (!name.equals(category.name)) return false;
        if (!score.equals(category.score)) return false;
        return detail != null ? detail.equals(category.detail) : category.detail == null;
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + score.hashCode();
        result = 31 * result + (detail != null ? detail.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Category{" +
                "name='" + name + '\'' +
                ", score=" + score +
                ", detail=" + detail +
                '}';
    }
}
