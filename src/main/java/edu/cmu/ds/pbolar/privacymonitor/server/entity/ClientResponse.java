package edu.cmu.ds.pbolar.privacymonitor.server.entity;

import edu.cmu.ds.pbolar.privacymonitor.server.entity.azure.ImageColour;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Pranav
 */
public class ClientResponse {
    
    private int status;
    private String message, people;
    private List<String> tags;
    private ImageColour colour;
    private String caption, isAdultContentWarning;
    private Set<String> celebrities, landmarks;
    
    public static final String SUCCESS_MESSAGE = "OK";

    public ClientResponse(int httpStatusCode, String message) {
        this.status = httpStatusCode;
        this.message = message;
    }

    public ClientResponse(int httpStatusCode, String message,
            String caption, List<String> tags, 
            Set<String> celebrities, Set<String> landmarks, 
            String isAdultContentWarning, ImageColour colour, String people) {
        this.status = httpStatusCode;
        this.message = message;
        this.caption = caption;
        this.tags = tags;
        this.landmarks = landmarks;
        this.celebrities = celebrities;
        this.colour = colour;
        this.isAdultContentWarning = isAdultContentWarning;
        this.people = people;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Set<String> getCelebrities() {
        return celebrities;
    }

    public void setCelebrities(Set<String> celebrities) {
        this.celebrities = celebrities;
    }

    public Set<String> getLandmarks() {
        return landmarks;
    }

    public void setLandmarks(Set<String> landmarks) {
        this.landmarks = landmarks;
    }

    public ImageColour getColour() {
        return colour;
    }

    public void setColour(ImageColour colour) {
        this.colour = colour;
    }

    public String getAdultContentWarning() {
        return isAdultContentWarning;
    }

    public void setAdultContent(String isAdultContentWarning) {
        this.isAdultContentWarning = isAdultContentWarning;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getIsAdultContentWarning() {
        return isAdultContentWarning;
    }

    public void setIsAdultContentWarning(String isAdultContentWarning) {
        this.isAdultContentWarning = isAdultContentWarning;
    }

    public String getPeople() {
        return people;
    }

    public void setPeople(String people) {
        this.people = people;
    }

    @Override
    public String toString() {
        return "ClientResponse{" + "status=" + status + ", caption=" + caption + ", tags=" + tags + '}';
    }

}
