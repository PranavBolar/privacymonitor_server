package edu.cmu.ds.pbolar.privacymonitor.server.model;

import com.google.gson.Gson;
import edu.cmu.ds.pbolar.privacymonitor.server.entity.AzureRequest_Url;
import edu.cmu.ds.pbolar.privacymonitor.server.entity.AzureResponse_Error;
import edu.cmu.ds.pbolar.privacymonitor.server.entity.ClientRequest;
import edu.cmu.ds.pbolar.privacymonitor.server.entity.ClientResponse;
import edu.cmu.ds.pbolar.privacymonitor.server.entity.RemoteServerResponse;
import edu.cmu.ds.pbolar.privacymonitor.server.entity.azure.AdultRating;
import edu.cmu.ds.pbolar.privacymonitor.server.entity.azure.AzImage_Response;
import edu.cmu.ds.pbolar.privacymonitor.server.entity.azure.Category;
import edu.cmu.ds.pbolar.privacymonitor.server.entity.azure.CategoryDetail;
import edu.cmu.ds.pbolar.privacymonitor.server.entity.azure.Person;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.bson.Document;

/**
 *
 * @author Pranav
 */
public class AzureImageFeaturizer {

    private static final String UTF8 = StandardCharsets.UTF_8.toString();
    private final Gson gson;

    public AzureImageFeaturizer(Gson gson) {
        this.gson = gson;
    }

    public AzureImageFeaturizer() {
        this.gson = new Gson();
    }

    public ClientResponse analyzeImage(ClientRequest request, Document log) throws IOException {
        RemoteServerResponse azureResponse;

        // Did the client send base64 data or an image's URL?
        if (request.isImageUrl()) {
            // Azure takes a JSON for queries with image URLs
            String azureRequestJson = gson.toJson(new AzureRequest_Url(request.getImage_url()));
            azureResponse = doPost(Constant.getAzureUri(), azureRequestJson);
        } else {
            // Azure needs an octet-stream for local files
            byte[] imagesBytes = Base64.getMimeDecoder().decode(request.getImage_base64());
            azureResponse = doPost(Constant.getAzureUri(), imagesBytes);
        }

        return analyzeImage(azureResponse, log);
    }

    public ClientResponse analyzeImage(RemoteServerResponse azureResponse, Document log) throws IOException {
        if (azureResponse.getStatus() == HttpURLConnection.HTTP_OK) {
            log.append(Constant.MDB_RESP_SIZE_KEY, azureResponse.getContent().getBytes().length);
            log.append(Constant.MDB_RESP_TIME_KEY, azureResponse.getResponseTime());
            return parseAzureData(azureResponse.getContent(), log);
        } else {
            // Azure wasn't happy with the image provided
            // Relay this information to the user
            AzureResponse_Error azError = gson.fromJson(azureResponse.getContent(), AzureResponse_Error.class);
            log.append(Constant.MDB_AZURE_ERROR_KEY, azError.getMessage()+" ["+azError.getCode()+"]");
            return new ClientResponse(azureResponse.getStatus(), azError.getMessage());
        }
    }

    private ClientResponse parseAzureData(String azJson, Document log) {
        AzImage_Response azImage = gson.fromJson(azJson, AzImage_Response.class);

        // Get some details out
        Map<CategoryDetail.ID, Set<String>> celebLandmarkMap = getCelebsAndLandmarks(azImage);

        // Log metrics
        recordRequestMetrics(azImage, celebLandmarkMap, log);

        // Extract the features that ought to show up on the phone
        return new ClientResponse(
                HttpURLConnection.HTTP_OK, ClientResponse.SUCCESS_MESSAGE,
                azImage.getDescription().getCaptions().get(0).getText(),
                azImage.getDescription().getTags(),
                celebLandmarkMap.get(CategoryDetail.ID.CELEBRITY),
                celebLandmarkMap.get(CategoryDetail.ID.LANDMARK),
                setAdultWarning(azImage), azImage.getColor(), 
                getPeopleDescription(azImage.getFaces())
        );

//        Potentially also use confidence labelled tags (v2?)
//        azImage.getTags().stream().map(tag -> tag.getName()).collect(Collectors.toList())
    }

    /**
     * 
     * @param people List of faces found in the photo
     * @return A human readable description of the people in the photo if any
     */
    private String getPeopleDescription(List<Person> people) {
        if (people.isEmpty()) {
            return "There aren't any discernable faces in this photo";
        }

        int numOfPeople = people.size();
        int numMale = 0, totalAge = 0;
        for (Person person : people) {
            if (person.isMale()) numMale++;
            totalAge += person.getAge();
        }
        
        String howManyPeopleMessage = (numOfPeople==1)? 
                "There is one person in the photo " : "There are " +people.size() +" people in the photo ";
        String genderBreakupMessage = String.format("[%d male(s), %d female(s)]. ", numMale, numOfPeople-numMale);
        String ageMessage = String.format("Their average age is about %d ", totalAge/numOfPeople);
        
        return howManyPeopleMessage + genderBreakupMessage + ageMessage;
    }
    
    /**
     * @param azImage The image from Azure
     * @return A Map of Celebrities and Landmarks
     */
    private Map<CategoryDetail.ID, Set<String>> getCelebsAndLandmarks(AzImage_Response azImage) {
        Set<String> celebs = new HashSet<>();
        Set<String> landmarks = new HashSet<>();

        for (Category category : azImage.getCategories()) {
            if (!category.hasDetail()) {
                continue;
            }

            if (category.getDetail().hasCelebs()) {
                celebs.addAll(category.getDetail().getCelebrities().stream()
                        .map(celeb -> celeb.getName()).collect(Collectors.toList()));
            }

            if (category.getDetail().hasLandmarks()) {
                landmarks.addAll(category.getDetail().getLandmarks().stream()
                        .map(lm -> lm.getName()).collect(Collectors.toList()));
            }
        }

        Map<CategoryDetail.ID, Set<String>> celebsLandmarks = new HashMap<>();
        celebsLandmarks.put(CategoryDetail.ID.CELEBRITY, celebs);
        celebsLandmarks.put(CategoryDetail.ID.LANDMARK, landmarks);
        return celebsLandmarks;
    }

    /**
     * 
     * @param azImage
     * @return A human readable sentence indicating the age appropriateness of an image
     */
    private String setAdultWarning(AzImage_Response azImage) {
        AdultRating adultRating = azImage.getAdult();
        if (adultRating.getAdultContent()) {
            return "Yes.";
        }
        if (adultRating.getRacyContent()) {
            return "Not explicitly so. However, it is suggestive.";
        }
        if (adultRating.getRacyScore() < 0.5 && adultRating.getRacyScore() > 0.2) {
            return "Not significantly.";
        }
        return "Not at all";
    }

    private void recordRequestMetrics(AzImage_Response azImage, Map<CategoryDetail.ID, Set<String>> clMap, Document log) {
        log.append(Constant.MDB_CAPTION_CONFIDENCE_KEY, azImage.getDescription().getCaptions().get(0).getConfidence());
        log.append(Constant.MDB_TAG_NUM_KEY, azImage.getTags().size());
        log.append(Constant.MDB_CELEB_KEY, clMap.get(CategoryDetail.ID.CELEBRITY));
        log.append(Constant.MDB_LANDMARK_KEY, clMap.get(CategoryDetail.ID.LANDMARK));

        int numOfFaces = 0;
        if (azImage.hasFaces()) {
            numOfFaces = azImage.getFaces().size();
        }

        log.append(Constant.MDB_FACES_NUM_KEY, numOfFaces);
    }

    private HttpURLConnection getConnection(String url) throws MalformedURLException, IOException {
        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();

        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        connection.setRequestProperty("Accept-Charset", UTF8);
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestProperty(Constant.AZURE_SUB_KEY_HEADER, Constant.AZURE_SUB_KEY);

        return connection;
    }

    /**
     * 
     * @param url URL to POST to
     * @param payload bit stream payload
     * @return The server's response
     * @throws MalformedURLException
     * @throws IOException 
     */
    private RemoteServerResponse doPost(String url, byte[] payload) throws MalformedURLException, IOException {
        HttpURLConnection connection = getConnection(url);
        connection.setRequestProperty("Content-Type", Constant.OCTET_STREAM_MIME);

        // Writing the bytes out
        DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream());
        dataOutputStream.write(payload);

        return getServerResponse(connection);
    }

    /**
     * 
     * @param url URL to POST to
     * @param payload JSON payload
     * @return The server's response
     * @throws MalformedURLException
     * @throws IOException 
     */
    private RemoteServerResponse doPost(String url, String jsonPayload) throws MalformedURLException, IOException {
        HttpURLConnection connection = getConnection(url);
        connection.setRequestProperty("Content-Type", Constant.JSON_MIME);

        // Using a PrintWriter with auto flush enabled
        try (PrintWriter writer = new PrintWriter(
                new OutputStreamWriter(connection.getOutputStream(), StandardCharsets.UTF_8), true)) {
            writer.write(jsonPayload);
        }

        return getServerResponse(connection);
    }

    private RemoteServerResponse getServerResponse(HttpURLConnection connection) throws IOException {
        // Okay. Time to get the response!
        long startTime = System.currentTimeMillis();
        int responseCode = connection.getResponseCode();
        long stopTime = System.currentTimeMillis();
        long responseTime = stopTime - startTime;
        InputStream iStream = (responseCode == HttpURLConnection.HTTP_OK)
                ? connection.getInputStream() : connection.getErrorStream();

        return new RemoteServerResponse(responseCode, responseTime, extractResponse(iStream));
    }

    private String extractResponse(InputStream iStream) throws IOException {
        StringBuilder sBuilder;
        // There should be only one line in the response, so we'll grab just that from the stream
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(iStream, UTF8))) {
            sBuilder = new StringBuilder();
            while (true) {
                String line = reader.readLine();
                if (line == null) {
                    break;
                }
                sBuilder.append(line);
            }
        }

        return sBuilder.toString();
    }
}
