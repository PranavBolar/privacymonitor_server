<%-- 
    Document   : index
    Created on : Mar 31, 2018, 2:08:26 AM
    Author     : Pranav
--%>

<%@page import="com.mongodb.client.model.BsonField"%>
<%@page import="org.bson.BsonString"%>
<%@page import="org.bson.BsonDocument"%>
<%@page import="com.mongodb.client.AggregateIterable"%>
<%@page import="com.mongodb.Block"%>
<%@page import="edu.cmu.ds.pbolar.privacymonitor.server.model.Constant"%>
<%@page import="com.mongodb.client.model.Accumulators"%>
<%@page import="com.mongodb.client.model.Filters"%>
<%@page import="com.mongodb.client.model.Aggregates"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Map"%>
<%@page import="edu.cmu.ds.pbolar.privacymonitor.server.PrivacyMontiorController"%>
<%@page import="com.mongodb.client.MongoCollection"%>
<%@page import="com.mongodb.client.MongoCursor"%>
<%@page import="org.bson.Document"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/main.css">
        <title>Privacy Monitor Dashboard</title>
    </head>
    <body>
        <%
            MongoCollection<Document> logs = (MongoCollection) request.getAttribute(PrivacyMontiorController.MONGO_COLLECTION_ATTR);

            // The following code represents concepts explained on the MongoDB Java driver's tutorial site
            // mongodb.github.io/mongo-java-driver/3.6/driver/tutorials/aggregation/
            
            // The unique _id field (primary key of the collection) is used for grouping to creat a group of all the docs
            Double avgLatency = logs.aggregate(Arrays.asList(
                    Aggregates.group("_id", new BsonField("avgLatency", new BsonDocument("$avg", new BsonString("$resp-time"))))
            )).first().getDouble("avgLatency");

            Double lowestCaptionConfidence = logs.aggregate(Arrays.asList(
                    Aggregates.group("_id", new BsonField("avgCapConf", new BsonDocument("$min", new BsonString("$caption-confidence"))))
            )).first().getDouble("avgCapConf");

            Integer maxNumOfTags = logs.aggregate(Arrays.asList(
                    Aggregates.group("_id", new BsonField("maxNumOfTags", new BsonDocument("$max", new BsonString("$tag-num"))))
            )).first().getInteger("maxNumOfTags");

        %>
        <header>
            <h1>Privacy Monitor Dashboard</h1>
        </header>

        <section id="content">
            <h3>Analytics</h3>
            <div class="log">
                <div class="logRow">
                    <div class="logKey">Average latency</div>
                    <div class="logValue"><%=avgLatency + " ms"%></div>
                </div>
                <div class="logRow">
                    <div class="logKey">Lowest caption confidence</div>
                    <div class="logValue"><%=lowestCaptionConfidence%></div>
                </div>
                <div class="logRow">
                    <div class="logKey">Maximum number of tags per query    </div>
                    <div class="logValue"><%=maxNumOfTags%></div>
                </div>
            </div>

            <h3>All Logs</h3>
            <div style="column-count: 3">
                <%  MongoCursor<Document> cursor = logs.find().iterator();
                    try {
                        while (cursor.hasNext()) {
                            Document log = cursor.next();
                %>

                <div class="log"> 
                    <%          for (Map.Entry<String, Object> logEntry : log.entrySet()) {%>
                    <div class="logRow">
                        <div class="logKey"><%=logEntry.getKey()%></div>
                        <div class="logValue"><%=logEntry.getValue()%></div>
                    </div>
                    <%}%>
                </div>

                <%    }
                    } finally {
                        cursor.close();
                    }
                %>
            </div>
        </section>
    </body>
</html>
