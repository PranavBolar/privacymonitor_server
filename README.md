# Privacy Monitor Server

Project 4 for 95-702.

## Introduction

This project houses the code that runs on a [Heroku instance](https://privacy-monitor.herokuapp.com/). The dashboard, with current usage stats is available [here](https://privacy-monitor.herokuapp.com/dashboard).
It communicates with Microsoft Azure to perform advanced computer vision, and with mobile clients for new jobs.

## Setup

Execute Maven
`mvn package`